//
//  Message.swift
//  TwitSplit
//
//  Created by Scor Doan on 6/24/18.
//  Copyright © 2018 Scor Doan. All rights reserved.
//

import Foundation


class Message: Codable {
    var id:Int = 0
    var message:String
    var created:Date
    var user:User
    
    init(id: Int = 0, text: String, created:Date = Date(), user:User) {
        self.id = id
        self.message = text
        self.created = created
        self.user = user
    }
}

extension Message: Equatable {
    static func == (lhs: Message, rhs: Message) -> Bool {
        return lhs.id == rhs.id
    }
}
