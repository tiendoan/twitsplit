//
//  TweeterViewController.swift
//  TwitSplit
//
//  Created by Scor Doan on 6/24/18.
//  Copyright © 2018 Scor Doan. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TweeterViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private let disposeBag = DisposeBag()
    var viewModel:TweeterViewModel!
    
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configTableView()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
    }
    
    func configTableView() {
        tableView.refreshControl = UIRefreshControl()
        tableView.estimatedRowHeight = 90
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    private func bindViewModel() {
        self.viewModel = TweeterViewModel(useCase: PersistenceManager.shared)
        let viewWillAppear = rx.sentMessage(#selector(UIViewController.viewWillAppear(_:)))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        let pull = tableView.refreshControl!.rx
            .controlEvent(.valueChanged)
            .asDriver()
        
        let input = TweeterViewModel.Input(trigger: Driver.merge(viewWillAppear, pull))
        let output = viewModel.transform(input: input)
        //Bind Messages to UITableView
        output.messages.drive(tableView.rx.items(cellIdentifier: R.reuseIdentifier.messageTableViewCell.identifier, cellType: MessageTableViewCell.self)) { tv, viewModel, cell in
            cell.bind(viewModel)
            
            }.disposed(by: disposeBag)
        
        output.fetching
            .drive(tableView.refreshControl!.rx.isRefreshing)
            .disposed(by: disposeBag)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Event methods
    @IBAction func addMessageClicked(_ sender: Any) {
        self.performSegue(withIdentifier: R.segue.tweeterViewController.toCreateMessage, sender: nil)
    }
}

