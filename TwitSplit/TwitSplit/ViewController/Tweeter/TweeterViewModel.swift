//
//  TweeterViewModel.swift
//  TwitSplit
//
//  Created by Scor Doan on 6/25/18.
//  Copyright © 2018 Scor Doan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol MessagesUseCase {
    func messages() -> Observable<[Message]>
    func save(messages: [Message]) -> Observable<Void>
    func delete(messages: [Message]) -> Observable<Void>
}


class TweeterViewModel: ViewModelType {
    
    struct Input {
        let trigger: Driver<Void>
    }
    struct Output {
        let fetching: Driver<Bool>
        let messages: Driver<[MessageItemViewModel]>
        let error: Driver<Error>
    }
    
    private let useCase: MessagesUseCase
    
    init(useCase: MessagesUseCase) {
        self.useCase = useCase
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        let messages = input.trigger.flatMapLatest {
            return self.useCase.messages()
                .trackActivity(activityIndicator)
                .trackError(errorTracker)
                .asDriverOnErrorJustComplete()
                .map { $0.map { MessageItemViewModel(with: $0) } }
        }
        
        let fetching = activityIndicator.asDriver()
        let errors = errorTracker.asDriver()
        
        return Output(fetching: fetching,
                      messages: messages,
                      error: errors)
    }
}

