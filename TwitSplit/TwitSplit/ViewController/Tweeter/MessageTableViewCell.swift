//
//  MessageTableViewCell.swift
//  TwitSplit
//
//  Created by Scor Doan on 6/25/18.
//  Copyright © 2018 Scor Doan. All rights reserved.
//

import Foundation
import UIKit

class MessageTableViewCell: UITableViewCell {
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.userImageView.layer.cornerRadius = self.userImageView.bounds.size.height/2
        self.userImageView.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    func bind(_ viewModel:MessageItemViewModel) {
        self.userImageView.image = UIImage(named: viewModel.userAvatar)
        self.userNameLabel.text = viewModel.userName
        self.dateLabel.text = viewModel.createDate
        self.messageLabel.text = viewModel.text
    }
}
