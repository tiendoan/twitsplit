//
//  MessageItemViewModel.swift
//  TwitSplit
//
//  Created by Scor Doan on 6/25/18.
//  Copyright © 2018 Scor Doan. All rights reserved.
//

import Foundation

class MessageItemViewModel   {
    let text:String
    let userName: String
    let userAvatar: String
    let createDate: String
    
    let message: Message
    init (with message: Message) {
        self.message = message
        self.text = message.message
        self.userName = message.user.name
        self.userAvatar = message.user.avatar
        self.createDate = " • " + message.created.timeAgoSince()
    }
}
