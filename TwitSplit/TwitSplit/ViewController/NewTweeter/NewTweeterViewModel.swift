//
//  NewTweeterViewModel.swift
//  TwitSplit
//
//  Created by Scor Doan on 6/25/18.
//  Copyright © 2018 Scor Doan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class NewTweeterViewModel {
    private let createMessageUseCase: MessagesUseCase
    struct Input {
        let cancelTrigger: Driver<Void>
        let postTrigger: Driver<Void>
        let text: Driver<String>
    }
    
    struct Output {
        let dismiss: Driver<Void>
        let canPost: Driver<Bool>
        let error: Driver<Error>
    }
    
    init(createMessageUseCase: MessagesUseCase) {
        self.createMessageUseCase = createMessageUseCase
    }
    
    func transform(input: Input) -> Output {
        let activityIndicator = ActivityIndicator()
        
        let canPost = Driver.combineLatest(input.text, activityIndicator.asDriver()) {
            return !$0.isEmpty && !$1
        }
        
//        let post = input.postTrigger.withLatestFrom(input.text)
//            .map { (text) in
//                return Message(text: text, user: PersistenceManager.shared.loggedUser)
//            }
//            .flatMapLatest { [unowned self] in
//                return self.createMessageUseCase.save(messages: [$0])
//                    .trackActivity(activityIndicator)
//                    .asDriverOnErrorJustComplete()
//        }
        let errorTracker = ErrorTracker()
        let post = input.postTrigger.withLatestFrom(input.text)
            //.asObservable()
            .flatMap { (text) in
                return splitMessageObservable(text, maxCharacters: AppDefined.Message.MAXIMUM_CHARACTER_ALLOWED)
                    .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
                    .map { (texts) in
                        return texts.map { return Message(text: $0, user: PersistenceManager.shared.loggedUser) }
                    }
            }
            .flatMapLatest { [unowned self] (messages) in
                return self.createMessageUseCase.save(messages: messages)
                    .trackActivity(activityIndicator)
                    .asDriverOnErrorJustComplete()
            }
    
        let errors = errorTracker.asDriver()
        
        let dismiss = Driver.of(post, input.cancelTrigger)
            .merge()
        
        return Output(dismiss: dismiss, canPost: canPost, error: errors)
    }
}
