//
//  NewTweeterViewController.swift
//  TwitSplit
//
//  Created by Scor Doan on 6/25/18.
//  Copyright © 2018 Scor Doan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

let TEXT_PLACEHOLDER = "What's on your mind?"
class NewTweeterViewController: UIViewController {
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var postButton: UIBarButtonItem!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var placeholderLabel: UILabel!
    
    private let disposeBag = DisposeBag()
    var viewModel:NewTweeterViewModel!
    
    //MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.height/2
        avatarImageView.layer.masksToBounds = true
        avatarImageView.image = UIImage(named: PersistenceManager.shared.loggedUser.avatar)

        textView.becomeFirstResponder()
        textView.rx.text.asObservable().subscribe(onNext: { (value) in
            self.placeholderLabel.isHidden = !value!.isEmpty
        }, onError: nil, onCompleted: nil, onDisposed: nil).disposed(by: disposeBag)
        
        // Do any additional setup after loading the view, typically from a nib.
        viewModel = NewTweeterViewModel(createMessageUseCase: PersistenceManager.shared)
        let input = NewTweeterViewModel.Input(cancelTrigger: cancelButton.rx.tap.asDriver(),
                                              postTrigger: postButton.rx.tap.asDriver(),
                                              text: textView.rx.text.orEmpty.asDriver())
        
        let output = viewModel.transform(input: input)
        output.dismiss.drive(onNext: { () in
            self.dismiss(animated: true, completion: nil)
        }, onCompleted: nil, onDisposed: nil)
            .disposed(by: disposeBag)

        output.canPost.drive(postButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        output.error.drive(onNext: { (error) in
            self.showAlert(title: "Invalid Message!", msg: (error as? TWError)?.message)
        }, onCompleted: nil, onDisposed: nil)
            .disposed(by: disposeBag)

    }

}
