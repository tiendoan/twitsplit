//
//  SplitMessage.swift
//  TwitSplit
//
//  Created by Scor Doan on 6/25/18.
//  Copyright © 2018 Scor Doan. All rights reserved.
//

import Foundation
import RxSwift

/** Split a string to parts based on maxCharacters input
 - Parameters:
    - message: input String
    - maxCharacters: limit characters in each of parts
 - Returns: Observable<[String]
 */

func splitMessageObservable(_ message: String, maxCharacters: Int) -> Observable<[String]> {
    return Observable.create({ observer -> Disposable in
        do {
            let results:[String] = try splitMessage(message, maxCharacters: maxCharacters)
            observer.onNext(results)
            observer.onCompleted()
        } catch let error {
            // .Error sequence will be automatically completed
            observer.onError(error)
        }
        
        return Disposables.create()
    })
}

/** Split a string to parts based on maxCharacters input
 - Parameters:
    - message: input String
    - maxCharacters: limit characters in each of parts
 - Returns: Array of String
 */
func splitMessage(_ message: String, maxCharacters: Int) throws -> [String] {
    var results = [String]()
    let words = message.components(separatedBy: .whitespacesAndNewlines)
    let lenght = message.count
    var sumOfParts = Int(ceil(Double(message.count)/Double(maxCharacters)))
    let numOfWords = words.count
    if lenght <= maxCharacters {
        return [message]
    }
    var message = "1/\(sumOfParts)"
    var indicator = 1
    var index = 0
    while index < numOfWords {
        var newMessage = message + " " + words[index]
        if newMessage.count > maxCharacters {
            results.append(message)
            indicator += 1
            
            message = "\(indicator)/\(sumOfParts)"
            newMessage = message + " " + words[index]
            if newMessage.count > maxCharacters {
                throw TWError(code: 0, message: "The message contains a span of non-whitespace characters longer than \(maxCharacters) characters")
            }
            index -= 1
        } else {
            message = newMessage
        }
        index += 1
        
        // wrong sumOfParts, check again
        if indicator > sumOfParts {
            sumOfParts += 1
            indicator = 1
            index = 0
            message = "1/\(sumOfParts)"
            results = []
        }
    }
    
    if index == numOfWords {
        results.append(message)
    }
    return results
}

func splitMessageForTest(_ message: String, maxCharacters: Int) -> Any? {
    var results = [String]()
    let words = message.components(separatedBy: .whitespacesAndNewlines)
    let lenght = message.count
    var sumOfParts = Int(ceil(Double(message.count)/Double(maxCharacters)))
    let numOfWords = words.count
    if lenght <= maxCharacters {
        return message
    }
    var message = "1/\(sumOfParts)"
    var indicator = 1
    var index = 0
    while index < numOfWords {
        var newMessage = message + " " + words[index]
        if newMessage.count > maxCharacters {
            results.append(message)
            indicator += 1
            
            message = "\(indicator)/\(sumOfParts)"
            newMessage = message + " " + words[index]
            if newMessage.count > maxCharacters {
                return nil
            }
            index -= 1
        } else {
            message = newMessage
        }
        index += 1
        
        // wrong sumOfParts, check again
        if indicator > sumOfParts {
            sumOfParts += 1
            indicator = 1
            index = 0
            message = "1/\(sumOfParts)"
            results = []
        }
    }
    
    if index == numOfWords {
        results.append(message)
    }
    return results
}


