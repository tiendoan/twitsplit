//
//  PersistenceManager.swift
//  TwitSplit
//
//  Created by Scor Doan on 6/25/18.
//  Copyright © 2018 Scor Doan. All rights reserved.
//

import Foundation
import RxSwift

class PersistenceManager {
    
    static let shared = PersistenceManager()
    var allMessages = [Message]()
    var loggedUser = User(id:999, name: "Tien Doan", avatar: "generic-avatar")
    
    fileprivate func getDocumentsDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    fileprivate var dummyDataFilePath: String {
        let url = URL(fileURLWithPath: self.getDocumentsDirectory())
        return url.appendingPathComponent("dummy_data").path
    }
    
    func loadMessagesDataFromFile(url: URL?) {
        if let url = url {
            if let data = try? Data(contentsOf: url) {
                let jsonDecoder = JSONDecoder()
                jsonDecoder.dateDecodingStrategy = .formatted(DateFormatter.twitDateFormatter)
                do {
                    let messages = try jsonDecoder.decode([Message].self, from: data)
                    self.allMessages = messages
                } catch let err {
                    print("Err", err)
                    print("loading fresh dummy")
                    loadFromDummyFile()
                }
            }
        }
    }
    
    func saveMessagesData() {
        // Transform array into data and save it into file
        let fileURL = URL(fileURLWithPath: dummyDataFilePath)
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(DateFormatter.twitDateFormatter)
        
        do {
            let data = try encoder.encode(allMessages)
            if FileManager.default.fileExists(atPath: fileURL.path) {
                try FileManager.default.removeItem(at: fileURL)
            }
            FileManager.default.createFile(atPath: fileURL.path, contents: data, attributes: nil)
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
    func loadMessagesData() {
        //Check existed file
        if !FileManager.default.fileExists(atPath: dummyDataFilePath) {
            //Load from dump file
            loadFromDummyFile()
        } else {
            self.loadMessagesDataFromFile(url: URL(fileURLWithPath: dummyDataFilePath))
        }
    }
    
    func addNewMessage(_ message: Message) {
        message.id = self.allMessages.count
        self.allMessages.append(message)
        self.saveMessagesData()
    }
    
    func deleteMessage(index: Int) {
        guard index < self.allMessages.count else { return }
        self.allMessages.remove(at: index)
        self.updateMessageIds()
        self.saveMessagesData()
    }
    
    func deleteMessage(_ message: Message) {
        guard let index = self.allMessages.index(of: message) else { return }
        self.deleteMessage(index: index)
    }
    
    func loadFromDummyFile() {
        self.loadMessagesDataFromFile(url: R.file.dummy_dataJson())
        self.saveMessagesData()
    }
    
    func updateMessageIds() {
        var index = 0
        for message in allMessages {
            message.id = index
            index += 1
        }
    }
}

extension PersistenceManager: MessagesUseCase {
    func messages() -> Observable<[Message]> {
        return Observable.of(self.allMessages)
    }
    
    func save(messages: [Message]) -> Observable<Void> {
        messages.forEach { (message) in
            self.addNewMessage(message)
        }
        return Observable.just(())
    }
    
    func delete(messages: [Message]) -> Observable<Void> {
        return Observable.just(())
    }
    
    
}
