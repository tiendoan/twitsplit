//
//  TWError.swift
//  TwitSplit
//
//  Created by Scor Doan on 6/25/18.
//  Copyright © 2018 Scor Doan. All rights reserved.
//

import Foundation

struct TWError: Swift.Error, LocalizedError {
    var code: Int
    var message: String

    var errorDescription: String? {
        return message
    }
}
