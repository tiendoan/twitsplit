//
//  AppDefine.swift
//  TwitSplit
//
//  Created by Scor Doan on 6/25/18.
//  Copyright © 2018 Scor Doan. All rights reserved.
//

import Foundation

struct AppDefined {
    
    private static func getStringValue(key: String) -> String {
        guard let info = Bundle.main.infoDictionary else { return ""}
        return info[key] as? String ?? ""
    }
    
    struct App {
        static var Version: String {
            return getStringValue(key: "CFBundleShortVersionString")
        }
    }
    
    
    struct Message {
        static let MAXIMUM_CHARACTER_ALLOWED = 50
    }
}

