//
//  TwitSplitTests.swift
//  TwitSplitTests
//
//  Created by Scor Doan on 6/24/18.
//  Copyright © 2018 Scor Doan. All rights reserved.
//

import XCTest
@testable import TwitSplit

class TwitSplitTests: XCTestCase {
    let input1 = "zalora"
    let expected1 = "zalora"
    
    let input2 = "abcdefghijilmnopqrstuvwxyz1234567890!@#$%^&<>?:][]"
    let expected2 = "abcdefghijilmnopqrstuvwxyz1234567890!@#$%^&<>?:][]"
    
    let input3 = "abcdefghijilmnopqrstuvwxyz1234567890!@#$%^&<>?:][]1"
    
    
    let input4 = "Twitter mostly uses Ruby on Rails for their front-end and primarily Scala and Java for back-end services. They use Apache Thrift (originally developed by Facebook) to communicate between different internal services."
    let expected4 = ["1/6 Twitter mostly uses Ruby on Rails for their",
                     "2/6 front-end and primarily Scala and Java for",
                     "3/6 back-end services. They use Apache Thrift",
                     "4/6 (originally developed by Facebook) to",
                     "5/6 communicate between different internal",
                     "6/6 services."]
    
    let input5 = "Apart from counting words and characters, our online editor can help you to improve word choice and writing style, and, optionally, help you to detect grammar mistakes and plagiarism. To check word count, simply place your cursor into the text box above and start typing. You'll see the number of characters and words increase or decrease as you type, delete, and edit them. You can also copy and paste text from another program over into the online editor above. The Auto-Save feature will make sure you won't lose any changes while editing, even if you leave the site and come back later. Tip: Bookmark this page now."
    let expected5 = ["1/15 Apart from counting words and characters, our",
                     "2/15 online editor can help you to improve word",
                     "3/15 choice and writing style, and, optionally,",
                     "4/15 help you to detect grammar mistakes and",
                     "5/15 plagiarism. To check word count, simply place",
                     "6/15 your cursor into the text box above and start",
                     "7/15 typing. You\'ll see the number of characters",
                     "8/15 and words increase or decrease as you type,",
                     "9/15 delete, and edit them. You can also copy and",
                     "10/15 paste text from another program over into",
                     "11/15 the online editor above. The Auto-Save",
                     "12/15 feature will make sure you won\'t lose any",
                     "13/15 changes while editing, even if you leave the",
                     "14/15 site and come back later. Tip: Bookmark this",
                     "15/15 page now."]
    
    let input6 = "abcdefghijilmnopqrstuvwxyz1234567890!@#$%^& abcdefghijilmnopqrstuvwxyz1234567890!@#$%^& abcdefghijilmnopqrstuvwxyz1234567890!@#$%^& abcdefghijilmnopqrstuvwxyz1234567890!@#$%^& abcdefghijilmnopqrstuvwxyz1234567890!@#$%^& abcdefghijilmnopqrstuvwxyz1234567890!@#$%^& abcdefghijilmnopqrstuvwxyz1234567890!@#$%^& abcdefghijilmnopqrstuvwxyz1234567890!@#$%^& abcdefghijilmnopqrstuvwxyz1234567890!@#$%^& abcdefghijilmnopqrstuvwxyz1234567890!@#$%^&22"
    
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testSplitMessage() {
        let maxCharacters = 50
        let input = "I can't believe Tweeter now supports chunking my messages, so I don't have to do it myself."
        let expected = ["1/2 I can't believe Tweeter now supports chunking", "2/2 my messages, so I don't have to do it myself."]
        let result = try! splitMessage(input, maxCharacters: maxCharacters)
        XCTAssertEqual(result, expected)
        
        //Testcase 1
        XCTAssertTrue(splitMessageForTest(input1, maxCharacters: maxCharacters) as! String == expected1)
        
        //Testcase 2
        XCTAssertTrue(splitMessageForTest(input2, maxCharacters: maxCharacters) as! String == expected2)
        
        //Testcase 3
        XCTAssertNil(splitMessageForTest(input3, maxCharacters: maxCharacters))

        //Testcase 4
        XCTAssertTrue(splitMessageForTest(input4, maxCharacters: maxCharacters) as! [String] == expected4)
        
        //Testcase 5
        XCTAssertTrue(splitMessageForTest(input5, maxCharacters: maxCharacters) as! [String] == expected5)
        
        //Testcase 6
        XCTAssertNil(splitMessageForTest(input6, maxCharacters: maxCharacters))
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
